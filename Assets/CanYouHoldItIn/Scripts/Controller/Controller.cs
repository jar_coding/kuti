﻿using UnityEngine.UI;

namespace CanYouHoldItIn.Scripts.Controller
{
	using System;
	using System.Linq;
	using System.Collections.Generic;
	using UnityEngine;
	using Settings;
	using States;

	public class Controller : MonoBehaviour
	{
		public Image title;
		public AudioSource music;
		
		public State initialState;
		public GameSettings settings;

		public Dictionary<EKutiButton, Player> Players { get; private set; }
		public List<Player> Winners { get; set; }
		
		private readonly Array buttons = Enum.GetValues(typeof(EKutiButton));
		private State currentState;

		private void Awake()
		{
			Winners = new List<Player>();
			Players = new Dictionary<EKutiButton, Player>();
		}

		private void Start()
		{
			foreach (var player in FindObjectsOfType<Player>()) {
				Players.Add(player.button, player);
			}

			SetState(initialState);
		}
		
		public void SetState(State state)
		{
			if(currentState != null) {
				currentState.OnStateExit();
			}

			currentState = state;
			currentState.SetController(this);

			if (currentState != null) {
				currentState.OnStateEnter();
			}
		}

		public void Update()
		{
			foreach (EKutiButton button in buttons) {
				if (KutiInput.GetKutiButtonDown(button)) {
					OnButtonPressed(button);
				}
			}
			
			currentState.UpdateState(Time.deltaTime);
		}

		public List<Player> ListOfAllPlayers()
		{
			return Players.Values.ToList();
		}

		private void OnButtonPressed(EKutiButton button)
		{
			currentState.OnButtonPressed(button);
		}
	}
}
