﻿namespace CanYouHoldItIn.Scripts
{
    using System;
    using UnityEngine;
    using DG.Tweening;

    public enum EffectName
    {
        FART,
        SPARKLE,
        CIRCLE_SPARKLE
    }

    [System.Serializable]
    public class ScaleEffect
    {
        public float amount;
        public float duration;
    }
    
    public static class EffectManager
    {
        public static void ZoomIn(Transform transform, float duration, float amount, float min)
        {
            transform.localScale = Vector3.one * min;
            Highlight(transform, duration, amount, min);
        }

        public static void ZoomOut(Transform transform, float duration, float amount)
        {
            var seq = DOTween.Sequence();
            seq.Append(transform.DOScale(transform.localScale.x + 0.2f, 0.1f));
            seq.Append(transform.DOScale(amount, duration));
        }

        public static void Highlight(Transform transform, float duration, float amount, float min)
        {
            float backDuration = duration / 10;
            var seq = DOTween.Sequence();
            seq.Append(transform.DOScale(amount, duration));
            seq.Append(transform.DOScale(min, backDuration));
        }

        public static void SpawnEffect(EffectName name, Transform transform)
        {
            GameObject prefab;
            
            switch (name) {
                case EffectName.FART:
                    prefab = Resources.Load<GameObject>("Prefabs/Fart");   
                    break;
                case EffectName.SPARKLE:
                    prefab = Resources.Load<GameObject>("Prefabs/Sparkles");
                    break;
                case EffectName.CIRCLE_SPARKLE:
                    prefab = Resources.Load<GameObject>("Prefabs/CircleSparkle");
                    break;
                default:
                    throw new ArgumentOutOfRangeException("name", name, null);
            }

            if (prefab != null) {
                UnityEngine.Object.Instantiate(prefab, transform);
            }
        }
    }
}