﻿using System.Collections;

namespace CanYouHoldItIn.Scripts.States
{
    using UnityEngine;
    
    [CreateAssetMenu(menuName = "States/DemoState")]
    public class DemoState : State
    {
        public float tickleChance = 0.5f;
        public float roundWinnerChance = 0.02f;
        public float tickleFrequenceMin = 1f;
        public float tickleFrequenceMax = 2f;
        
        private bool isRunning;
            
        public override void OnStateEnter()
        {
            EventManager.TriggerEvent(EventName.CLEAR_UI);
			ResetPlayers();
			
            isRunning = true;
            controller.StartCoroutine(PlayDemo());
        }

		private void ResetPlayers() {
			foreach (Player player in controller.ListOfAllPlayers()) {
				player.ButtonPressedCount = 0;
				player.Score = 0;
				player.mergedReactTime = 0;
			    EventManager.TriggerEvent(EventName.PLAYER_RESET, player);
			}
		}

		public override void OnStateExit()
        {
            isRunning = false;
        }

        private IEnumerator PlayDemo()
        {
            while (isRunning) {
                foreach (var player in controller.ListOfAllPlayers()) {
                    if (Random.value <= tickleChance) {
                        yield return new WaitForSeconds(Random.Range(0.2f, 0.5f));
                        EventManager.TriggerEvent(EventName.PLAYER_PRESSED, player);

                    } else if (Random.value <= roundWinnerChance) {
                        EventManager.TriggerEvent(EventName.ROUND_WINNER_IS, player);
                        yield return new WaitForSeconds(1f);

                    } else {
                        yield return new WaitForSeconds(0.5f);
                        EventManager.TriggerEvent(EventName.PLAYER_RESET, player);
                    }
                }
                
                yield return new WaitForSeconds(Random.Range(tickleFrequenceMin, tickleFrequenceMax));
            }
        }

        public override void OnButtonPressed(EKutiButton button)
        {
            controller.SetState(nextState);
        }
    }
}