﻿using System.Linq;
using DG.Tweening;

namespace CanYouHoldItIn.Scripts.States
{
	using System.Collections;
	using UnityEngine;
	
	[CreateAssetMenu(menuName = "States/GameOverState")]
	public class GameOverState : State
	{
		private bool restartable = false;
		
		public override void OnStateEnter()
		{
			restartable = false;
			EventManager.TriggerEvent(EventName.GAME_OVER_ENTER);
			
			EventManager.TriggerEvent(EventName.GAME_WINNER_IS, GetGameWinner());
			controller.StartCoroutine(WaitBeforeStartNextState());
		}

		public override void OnStateExit()
		{
			EventManager.TriggerEvent(EventName.CLEAR_UI);
			controller.music.Stop();
		}

		private Player GetGameWinner()
		{
			float min = controller.Winners.Min(player => player.mergedReactTime);
			return controller.Winners.First(entry => entry.mergedReactTime <= min);
		}
		
		private IEnumerator WaitBeforeStartNextState()
		{
			yield return new WaitForSeconds(controller.settings.delayAfterGameOver);
			EventManager.TriggerEvent(EventName.GAME_READY_TO_RESTART);
			restartable = true;
		}

		public override void OnButtonPressed(EKutiButton button)
		{
			if (!restartable) {
				return;
			}
			
			EventManager.TriggerEvent(EventName.GAME_OVER_EXIT);
			controller.SetState(nextState);
		}
	}
}
