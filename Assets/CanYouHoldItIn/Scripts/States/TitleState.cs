using System.Collections;

namespace CanYouHoldItIn.Scripts.States
{
    using UnityEngine;

    [CreateAssetMenu(menuName = "States/TitleState")]
    public class TitleState : State
    {
	    public override void OnStateEnter()
        {
            EventManager.TriggerEvent(EventName.CLEAR_UI);
			ResetPlayers();

	        controller.StartCoroutine(Wait());
        }

		private void ResetPlayers() {
			foreach (Player player in controller.ListOfAllPlayers()) {
				player.ButtonPressedCount = 0;
				player.Score = 0;
				player.mergedReactTime = 0;
			    EventManager.TriggerEvent(EventName.PLAYER_RESET, player);
			}
		}

	    private IEnumerator Wait()
	    {
		    yield return new WaitForSeconds(0.1f);
		    controller.title.enabled = true;
	    }

		public override void OnStateExit()
		{
			controller.title.enabled = false;
			controller.music.Play();
		}

        public override void OnButtonPressed(EKutiButton button)
        {
            controller.SetState(nextState);
        }
	}
}