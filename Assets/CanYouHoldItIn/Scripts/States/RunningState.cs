﻿namespace CanYouHoldItIn.Scripts.States
{
	using UnityEngine;

	[CreateAssetMenu(menuName = "States/RunningState")]
	public class RunningState : State
	{
		private float remainingTime;
		private float currentTime;
		
		public override void OnStateEnter()
		{
			currentTime = 0;
			remainingTime = controller.settings.playTime;
			EventManager.TriggerEvent(EventName.RUNNING);
		}

		public override void UpdateState(float deltaTime)
		{
			currentTime += deltaTime;
			
			if (RoundOver()) {
				controller.SetState(nextState);
				return;
			}
			
			float value = controller.settings.progress.Evaluate(remainingTime / controller.settings.playTime);
			EventManager.TriggerEvent(EventName.PROGRESS, value);

			remainingTime -= deltaTime;
		}

		private bool RoundOver()
		{
			return remainingTime < 0;
		}

		public override void OnButtonPressed(EKutiButton button)
		{
			var player = controller.Players[button];
			player.ButtonPressedCount++;
			player.lastReactTime = currentTime;
			
			EventManager.TriggerEvent(EventName.PLAYER_PRESSED, player);
		}
	}
}
