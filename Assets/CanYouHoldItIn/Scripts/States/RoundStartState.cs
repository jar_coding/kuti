﻿namespace CanYouHoldItIn.Scripts.States
{
	using UnityEngine;

	[CreateAssetMenu(menuName = "States/RoundStartState")]
	public class RoundStartState : State
	{
		private float currentTime;
		
		public override void OnStateEnter()
		{
			ResetPlayers();
			controller.Winners.Clear();
			currentTime = 0;
			
			EventManager.TriggerEvent(EventName.ROUND_START);
		}

		private void ResetPlayers()
		{
			foreach (var player in controller.ListOfAllPlayers()) {
				player.ButtonPressedCount = 0;
				player.lastReactTime = 0;
			}
		}

		public override void OnButtonPressed(EKutiButton button)
		{
			Player player = controller.Players[button];
			player.ButtonPressedCount--;
			
			EventManager.TriggerEvent(EventName.PLAYER_PRESSED, player);
		}

		public override void UpdateState(float deltaTime)
		{
			currentTime += deltaTime;
			float value = controller.settings.progress.Evaluate(currentTime / controller.settings.timeToFillProgress);
			
			if (value >= 1) {
				controller.SetState(nextState);
			}

			EventManager.TriggerEvent(EventName.PROGRESS, value);
		}
	}
}
