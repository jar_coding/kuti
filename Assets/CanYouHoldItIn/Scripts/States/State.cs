﻿namespace CanYouHoldItIn.Scripts.States
{
	using UnityEngine;
	using Controller;

	public abstract class State : ScriptableObject
	{
		public State nextState;

		protected Controller controller;

		public void SetController(Controller c)
		{
			controller = c;
		}

		public virtual void UpdateState(float deltaTime) { }
		public virtual void OnButtonPressed(EKutiButton button) { }
		public virtual void OnStateEnter() {}
		public virtual void OnStateExit() {}
	}
}
