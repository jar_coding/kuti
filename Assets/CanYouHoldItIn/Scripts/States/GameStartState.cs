﻿namespace CanYouHoldItIn.Scripts.States
{
	using UnityEngine;

	[CreateAssetMenu(menuName = "States/GameStartState")]
	public class GameStartState : State
	{
		public override void OnStateEnter()
		{
			controller.StopAllCoroutines();
			ResetPlayers();
			controller.Winners.Clear();
			
			EventManager.TriggerEvent(EventName.GAME_START_ENTER);
		}

		public override void OnStateExit()
		{
			EventManager.TriggerEvent(EventName.GAME_START_EXIT);
		}

		private void ResetPlayers()
		{
			foreach(Player player in controller.ListOfAllPlayers()) {
				player.ButtonPressedCount = 0;
				player.Score = 0;
				player.mergedReactTime = 0;
			}
		}

		public override void OnButtonPressed(EKutiButton button)
		{
			controller.SetState(nextState);
		}
	}
}
