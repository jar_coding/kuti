﻿namespace CanYouHoldItIn.Scripts.States
{
	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;
	using System.Linq;

	[CreateAssetMenu(menuName = "States/RoundOverState")]
	public class RoundOverState : State
	{
		public GameOverState gameOverState;
		private bool isReadyToRestart;

		private List<Player> winners;

		private void Awake()
		{
			winners = new List<Player>();
		}

		public override void OnStateEnter()
		{
			EventManager.TriggerEvent(EventName.ROUND_OVER_ENTER);
			isReadyToRestart = false;
			
			FindWinners();
			UpdateWinners();
			
			if (IsGameOver()) {
				foreach (Player winner in winners.Where(player => player.Score == controller.settings.numberOfPointsToWin)) {
					controller.Winners.Add(winner);
				}
				
				controller.SetState(gameOverState);
			} else {
				
				foreach(Player winner in winners) {
					controller.Winners.Add(winner);
					EventManager.TriggerEvent(EventName.ROUND_WINNER_IS, winner);
				}
				
				controller.StartCoroutine(WaitSomeSecondsBeforeStartNextState());
			}
		}

		private void FindWinners()
		{
			int max = controller.ListOfAllPlayers()
				.Where(x => x.ButtonPressedCount >= 1)
				.Select(x => x.ButtonPressedCount)
				.DefaultIfEmpty(1)
				.Max();
			
			var allPlayersWithHighestButtonPressedCount = 
				controller.ListOfAllPlayers()
					.FindAll(player => player.ButtonPressedCount >= max);
			
			float min = allPlayersWithHighestButtonPressedCount
				.Select(x => x.lastReactTime)
				.DefaultIfEmpty(0)
				.Min();
			
			winners = allPlayersWithHighestButtonPressedCount
				.FindAll(player => player.lastReactTime <= min);
		}

		private void UpdateWinners()
		{
			foreach(Player winner in winners) {
				winner.Score++;
				winner.mergedReactTime += winner.lastReactTime;
			}
		}

		public override void OnButtonPressed(EKutiButton button)
		{
			if (!isReadyToRestart) {
				return;
			}

			EventManager.TriggerEvent(EventName.ROUND_OVER_EXIT);
			controller.SetState(nextState);
		}

		private bool IsGameOver()
		{
			return winners.Exists(p => p.Score >= controller.settings.numberOfPointsToWin);
		}
		
		private IEnumerator WaitSomeSecondsBeforeStartNextState()
		{
			yield return new WaitForSeconds(controller.settings.delayBetweenRounds);
			EventManager.TriggerEvent(EventName.ROUND_RESTART);
			isReadyToRestart = true;
		}
	}
}
