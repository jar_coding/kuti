﻿namespace CanYouHoldItIn.Scripts
{ 
	using UnityEngine;

	public class Player : MonoBehaviour
	{
		public EKutiButton button;
		
		public int ButtonPressedCount { get; set; }
		public int Score { get; set; }

		[HideInInspector]
		public float lastReactTime;
		[HideInInspector]
		public float mergedReactTime;

		public override string ToString()
		{
			return "Player (" + button + ") : " + ButtonPressedCount +
			       " ReactTime " + lastReactTime +
			       " MergedReactTime " + mergedReactTime;
		}
	}
}
