﻿namespace CanYouHoldItIn.Scripts.Effects
{
	using UnityEngine;

	[CreateAssetMenu(menuName = "Effects/AudioEffect")]
	public class AudioEffect : ScriptableObject
	{
		public AudioClip[] clips;

		[Header("Settings")]
		[Range(0,1)]
		public float volume = 1;
		[Range(-3, 3)]
		public float pitch = 1;

		public bool hasRandomPitch;

		public void Play(AudioSource source)
		{
			if (source.isPlaying) return;
			
			source.clip = clips[Random.Range(0, clips.Length)];
			source.volume = volume;
			source.pitch = hasRandomPitch ? pitch + Random.Range(-.05f,.05f) : pitch;
			source.Play();
		}
	}
}
