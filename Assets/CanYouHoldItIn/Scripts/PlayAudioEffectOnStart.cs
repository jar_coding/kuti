﻿using CanYouHoldItIn.Scripts.Effects;
using UnityEngine;

namespace CanYouHoldItIn.Scripts
{
    [RequireComponent(typeof(AudioSource))]
    public class PlayAudioEffectOnStart : MonoBehaviour
    {
        public AudioEffect audioEffect;
        
        private void Start()
        {
            audioEffect.Play(GetComponent<AudioSource>());
        }
    }
}