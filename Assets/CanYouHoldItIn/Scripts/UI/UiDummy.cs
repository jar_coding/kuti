﻿namespace CanYouHoldItIn.Scripts.UI
{
    using UnityEngine;
    using Settings;
    using Effects;
    
    public class UiDummy : MonoBehaviour
    {
        public DummySettings settings;

        private SpriteRenderer spriteRenderer;
    
        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            transform.localScale = Vector3.zero;
        }

        public void UpdateScore(int score)
        {
            if (score == 0) {
                spriteRenderer.sprite = null;
                return;
            }
            
            EffectManager.Highlight(transform, 0.2f, 1.5f, 1f);
            
            spriteRenderer.sprite =
                settings.dummies[Mathf.Clamp(score - 1, 0, settings.dummies.Length-1)];
        }
    }
}



