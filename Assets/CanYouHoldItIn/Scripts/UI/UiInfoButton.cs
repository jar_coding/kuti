﻿
namespace CanYouHoldItIn.Scripts.UI
{
    using Effects;
    using UnityEngine;
    
    public class UiInfoButton : MonoBehaviour
    {
        public AudioEffect showButtonAudio;
        public AudioEffect startRunningAudio;
        
        private Animator animator;
        private AudioSource audioSource;

        private void Awake()
        {
            animator = GetComponent<Animator>();
            audioSource = GetComponent<AudioSource>();
        }
        
        private void Start()
        {
            transform.localScale = Vector3.zero;
            EventManager.StartListening(EventName.CLEAR_UI, OnClearUi);
            EventManager.StartListening(EventName.RUNNING, OnRunning);
            EventManager.StartListening(EventName.ROUND_OVER_ENTER, OnRoundOverEnter);
            EventManager.StartListening(EventName.ROUND_OVER_EXIT, OnRoundOverExit);
            EventManager.StartListening(EventName.ROUND_START, OnRoundStart);
            EventManager.StartListening(EventName.ROUND_RESTART, OnRoundRestart);
            EventManager.StartListening(EventName.GAME_READY_TO_RESTART, OnGameRestart);
            EventManager.StartListening(EventName.GAME_START_ENTER, OnGameStartEnter);
            EventManager.StartListening(EventName.GAME_START_EXIT, OnGameStart);
            EventManager.StartListening(EventName.GAME_OVER_EXIT, OnGameOverExit);
        }

        private void OnGameOverExit()
        {
            Hide();
            showButtonAudio.Play(audioSource);
        }

        private void OnClearUi()
        {
            Hide();
        }

        private void OnGameStartEnter()
        {
            MultiplyAnimationSpeed(1f);
            Show();
            showButtonAudio.Play(audioSource);
        }

        private void OnRoundStart()
        {
            Hide();
        }

        private void OnRoundRestart()
        {
            Show();
            showButtonAudio.Play(audioSource);
        }

        private void OnRoundOverExit()
        {

            Hide();
        }

        private void OnGameRestart()
        {
            Show();
            showButtonAudio.Play(audioSource);
        }

        private void OnGameStart()
        {
            Hide();
        }

        private void OnRunning()
        {
            startRunningAudio.Play(audioSource);
            MultiplyAnimationSpeed(5f);
            EffectManager.ZoomIn(transform, 0.3f, 2.5f, 1.2f);
        }

        private void OnRoundOverEnter()
        {
            MultiplyAnimationSpeed(1f);
            Hide();
        }

        private void Show()
        {
            EffectManager.ZoomIn(transform, 0.05f, 1.7f, 1.2f);
        }

        private void Hide()
        {
            EffectManager.ZoomOut(transform, 0.2f, 0);
        }

        private void MultiplyAnimationSpeed(float amount)
        {
            animator.SetFloat("speed", amount);
        }
    }
}