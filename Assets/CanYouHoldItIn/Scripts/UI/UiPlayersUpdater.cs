﻿namespace CanYouHoldItIn.Scripts.UI
{
    using UnityEngine;
    
    public class UiPlayersUpdater : MonoBehaviour
    {
        private void Start()
        {
            EventManager.StartListening(EventName.PLAYER_PRESSED, OnPlayerPressed);
            EventManager.StartListening(EventName.ROUND_WINNER_IS, OnRoundWinnerIs);
            EventManager.StartListening(EventName.GAME_WINNER_IS, OnGameWinnerIs);
            EventManager.StartListening(EventName.PLAYER_RESET, OnPlayerReset);
        }

        private static void OnPlayerReset(Player player)
        {
            UiPlayerUpdater updater = player.GetComponent<UiPlayerUpdater>();
            
            if (updater == null) {
                return;
            }

            updater.Reset();
        }

        private static void OnGameWinnerIs(Player player)
        {
            Instantiate(Resources.Load<GameObject>("Prefabs/Crown"), player.transform);
            OnRoundWinnerIs(player);
        }

        private static void OnRoundWinnerIs(Player player)
        {
            UiPlayerUpdater updater = player.GetComponent<UiPlayerUpdater>();

            if (updater == null) {
                return;
            }

            updater.HighlightWinner();
            updater.UpdateScore(player.Score);
        }

        private static void OnPlayerPressed(Player player)
        {
            UiPlayerUpdater updater = player.GetComponent<UiPlayerUpdater>();

            if (updater != null) {
                updater.ButtonPressed();
            }
        }
    }
}