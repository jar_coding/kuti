﻿namespace CanYouHoldItIn.Scripts.UI {

	using UnityEngine;
	using UnityEngine.UI;
	
	public class UiProgressBar : MonoBehaviour
	{
		public Image left;
		public Image right;
		public ScaleEffect effect;
		
		private void Start()
		{
			EventManager.StartListening(EventName.PROGRESS, UpdateProgress);
			EventManager.StartListening(EventName.ROUND_START, Show);
			EventManager.StartListening(EventName.ROUND_OVER_ENTER, Hide);
			EventManager.StartListening(EventName.RUNNING, Hightlight);
			gameObject.SetActive(false);
		}

		private void UpdateProgress(float amount)
		{
			left.fillAmount = amount;
			right.fillAmount = amount;
		}

		private void Show()
		{
			gameObject.SetActive(true);
			//EffectManager.ZoomIn(transform, 0.3f, 11f, 10f);
		}

		private void Hide()
		{
			gameObject.SetActive(false);
			EffectManager.ZoomOut(transform, effect.duration, effect.amount);
		}

		private void Hightlight()
		{
			EffectManager.Highlight(
				transform,
				0.2f,
				effect.amount * 1.1f,
				effect.amount);
		}
	}
}
