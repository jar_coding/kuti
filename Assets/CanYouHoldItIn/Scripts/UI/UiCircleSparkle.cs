﻿namespace CanYouHoldItIn.Scripts.UI {
	
	using UnityEngine;
	using System.Collections;
	using DG.Tweening;

	[System.Serializable]
	public class SpriteSettings
	{
		public Sprite sprite;
		public Color color;
		public float scale;
		public float duration;
	}
	
	[RequireComponent(typeof(SpriteRenderer))]
	public class UiCircleSparkle : MonoBehaviour
	{
		public SpriteSettings[] sprites;

		private int index;
		private SpriteRenderer spriteRenderer;
		private AudioSource audioSource;
		
		private void Start ()
		{
			spriteRenderer = GetComponent<SpriteRenderer>();
			audioSource = GetComponent<AudioSource>();
			
			StartCoroutine(Effect());
		}

		private IEnumerator Effect()
		{
			while (index < sprites.Length) {

				audioSource.Play();
				
				SpriteSettings spriteSettings = sprites[index];
				spriteRenderer.sprite = spriteSettings.sprite;
				Color color = spriteSettings.color;
				
				spriteRenderer.color = spriteSettings.color;
				transform.localScale = Vector3.one;

				float duration = spriteSettings.duration;
				
				transform.DOScale(spriteSettings.scale, duration);
				spriteRenderer.DOColor(new Color(color.r, color.g, color.b, 0), duration);
				index++;
				
				yield return new WaitForSeconds(duration);
			}
			
			Destroy(gameObject);
		}
	}
}
