﻿namespace CanYouHoldItIn.Scripts.UI
{
	using UnityEngine;
	using UnityEngine.UI;
	using DG.Tweening;
	
	public class UiOverlay : MonoBehaviour
	{
		public float fadeInDuration = 0.2f;
		public float fadeOutDuration = 0.1f;
		
		private Image overlayImage;
		private Sequence sequence;
		
		private void Start()
		{
			overlayImage = GetComponent<Image>();
			
			EventManager.StartListening(EventName.GAME_START_ENTER, Flash);
			EventManager.StartListening(EventName.GAME_OVER_EXIT, Flash);
			EventManager.StartListening(EventName.CLEAR_UI, Flash);
			
			sequence = DOTween.Sequence();
			
			sequence.Append(overlayImage.DOFade(1f, fadeInDuration));
			sequence.Append(overlayImage.DOFade(0f, fadeOutDuration));
			sequence.Pause();

			overlayImage.DOFade(0f, 0f);
		}

		private void Flash()
		{
			sequence.Play().OnComplete(() =>
			{
				sequence.Rewind();
			});
		}
	}
}
