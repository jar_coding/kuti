﻿namespace CanYouHoldItIn.Scripts.UI
{
	using UnityEngine;

	public class UiFace : MonoBehaviour {

		private Animator animator;
		public SpriteRenderer SpriteRenderer { get; private set; }

		private const string Speed = "animationSpeed";
		private const string IsAwake = "isAwake";
		private const string IsCrying = "isCrying";
		private const string IsScreaming = "isScreaming";
		private const string IsLaughting = "isLaughting";
		
		private void Awake()
		{
			animator = GetComponent<Animator>();
			animator.SetFloat(Speed, Random.Range(1, 1.2f));
			SpriteRenderer = GetComponent<SpriteRenderer>();
		}

		public void SetAwake(bool isAwake) {
			animator.SetBool(IsAwake, isAwake);
		}

		public void SetCrying(bool isCrying)
		{
			SetAwake(true);
			animator.SetBool(IsCrying, isCrying);
		}
		
		public void SetScreaming(bool isScreaming)
		{
			SetAwake(true);
			animator.SetBool(IsScreaming, isScreaming);
		}
		
		public void SetLaughting(bool isLaughting)
		{
			SetAwake(true);
			animator.SetBool(IsLaughting, isLaughting);
		}

		public void Reset()
		{
			SetAwake(false);
			SetCrying(false);
			SetLaughting(false);
			SetScreaming(false);
		}
	}
}
