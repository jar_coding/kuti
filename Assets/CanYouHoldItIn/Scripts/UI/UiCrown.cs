﻿

using DG.Tweening;

namespace CanYouHoldItIn.Scripts.UI
{
    using UnityEngine;
    
    public class UiCrown : MonoBehaviour
    {
        private void Start()
        {
            transform.localPosition = Vector3.up * 2f;
            transform.localScale = Vector3.zero;
            
            var seq = DOTween.Sequence();
            seq.Append(transform.DOScale(1.05f, 0.4f));
            seq.Append(transform.DOLocalMoveY(1.02f, 0.1f));
            seq.Append(transform.DOScale(1.3f, 0.2f));
            seq.Append(transform.DOScale(1.05f, 0.1f));
            
            EventManager.StartListening(EventName.GAME_START_ENTER, StopListeningAndDestroy);
            EventManager.StartListening(EventName.CLEAR_UI, StopListeningAndDestroy);
        }

        private void StopListeningAndDestroy()
        {
            transform.DOScale(0, 0.2f).OnComplete(() =>
            {
                EventManager.StopListening(EventName.GAME_START_ENTER, StopListeningAndDestroy);
                EventManager.StopListening(EventName.CLEAR_UI, StopListeningAndDestroy);
                Destroy(gameObject);
            });
        }
    }
}