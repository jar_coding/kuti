﻿using CanYouHoldItIn.Scripts.Settings;
using UnityEngine.UI;

namespace CanYouHoldItIn.Scripts.UI
{
	using UnityEngine;

	[RequireComponent(typeof(Player))]
	public class UiPlayerUpdater : MonoBehaviour
	{
		public PlayerSettings settings;
		public Image blanket;
		
		private AudioSource audioSource;
		private Animator animator;

		private Material blanketMaterial;
		private SpriteRenderer spriteRenderer;
		private UiFace uiFace;
		private UiDummy dummy;

		private bool isWinner;
		private float currentTime;
		
		private void Awake() 
		{
			audioSource = GetComponent<AudioSource>();
			animator = GetComponent<Animator>();
			animator.SetFloat("speedMultiplier", Random.Range(1.4f, 2f));
			uiFace = transform.Find("Face").GetComponent<UiFace>();
			dummy = uiFace.transform.Find("Dummy").GetComponent<UiDummy>();
			spriteRenderer = GetComponent<SpriteRenderer>();
		}

		private void Start() 
		{
			EventManager.StartListening(EventName.GAME_START_ENTER, OnGameStarts);
			EventManager.StartListening(EventName.ROUND_OVER_ENTER, OnRoundOverEnter);
			EventManager.StartListening(EventName.ROUND_START, OnRoundStarts);
			EventManager.StartListening(EventName.CLEAR_UI, ClearUi);
		}

		private void Update()
		{
			currentTime += Time.deltaTime;
			
			if (!isWinner) {
				return;
			}

			float scaling = settings.highlightWinner.Evaluate(currentTime);
			transform.localScale = Vector3.one * scaling;
		}

		private void ClearUi()
		{
			dummy.UpdateScore(0);
			Reset();
		}

		public void Reset()
		{
			audioSource.Stop();
			ChangeSaturation(0f);
			uiFace.Reset();
			isWinner = false;
			currentTime = 0;
			uiFace.SetLaughting(false);
			EffectManager.ZoomIn(transform, 0.2f, settings.startScale, settings.startScale);
			//transform.localScale = Vector3.one * settings.startScale;
			animator.SetBool("winning", isWinner);
		}

		public void ButtonPressed()
		{
			if (currentTime >= settings.tickleScaling.duration) {
				EffectManager.Highlight(transform,
					settings.tickleScaling.duration,
					settings.tickleScaling.amount,
					settings.startScale);
				currentTime = 0;
			}
			
			uiFace.SetLaughting(true);
			
			if (Random.value >= settings.fartProbability) {
				EffectManager.SpawnEffect(EffectName.FART, transform);
				audioSource.Stop();
				return;
			}
			
			animator.SetTrigger("tickle");
			settings.laughtingAudio.Play(audioSource);
		}

		private void OnRoundStarts()
        {
	        Reset();
        }

		private void OnRoundOverEnter()
		{
			HighlightLoser();
		}

		public void HighlightWinner()
		{
			currentTime = 0;
			ChangeSaturation(0f);
			uiFace.SetLaughting(true);
			settings.laughtingAudio.Play(audioSource);
			isWinner = true;
			EffectManager.SpawnEffect(EffectName.SPARKLE, transform);
			EffectManager.SpawnEffect(EffectName.CIRCLE_SPARKLE, transform);
			animator.SetBool("winning", isWinner);
		}

		private void HighlightLoser()
		{
			ChangeSaturation(settings.minimalSaturation);
			uiFace.SetLaughting(false);

			if (Random.value >= 0.5f) {
				uiFace.SetCrying(true);
			} else {
				uiFace.SetScreaming(true);
			}

			audioSource.Stop();

			if (Random.value >= 0.5f) {
				settings.cryingAudio.Play(audioSource);
			}
		}

		private void OnGameStarts()
		{
			OnRoundStarts();
			uiFace.SetAwake(false);
			dummy.UpdateScore(0);
		}

		public void UpdateScore(int score)
		{
			dummy.UpdateScore(score);
		}

        private void ChangeSaturation(float saturation)
        {
	        spriteRenderer.material.SetFloat("_EffectAmount", saturation);
	        uiFace.SpriteRenderer.material.SetFloat("_EffectAmount", saturation);

	        if (blanketMaterial == null) {
				blanketMaterial = Instantiate(blanket.material);
	        }
	        
	        blanketMaterial.SetFloat("_EffectAmount", saturation);
	        blanket.material = blanketMaterial;
        }
	}
}