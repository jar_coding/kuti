﻿
namespace CanYouHoldItIn.Scripts
{
	using System.Collections.Generic;
	using System;

	public class EventManager
	{
		private readonly Dictionary<EventName, Action<float>> intDictionary;
		private readonly Dictionary<EventName, Action<Player>> playerDictionary;
		private readonly Dictionary<EventName, Action> voidDictionary;

		private static EventManager eventManager = new EventManager();

		private EventManager() 
		{
			intDictionary = new Dictionary<EventName, Action<float>>();
			playerDictionary = new Dictionary<EventName, Action<Player>>();
			voidDictionary = new Dictionary<EventName, Action>();
		}

		private static EventManager Instance {
			get { return eventManager ?? (eventManager = new EventManager()); }
		}
		
		public static void StartListening(EventName name, Action listener) 
		{
			Action action;
			if(Instance.voidDictionary.TryGetValue(name, out action)) {
				action += listener;
				Instance.voidDictionary[name] = action;
			} else {
				action += listener;
				Instance.voidDictionary.Add(name, action);
			}
		}

		public static void StartListening(EventName name, Action<Player> listener) 
		{
			Action<Player> action;
			if (Instance.playerDictionary.TryGetValue(name, out action)) {
				action += listener;
				Instance.playerDictionary[name] = action;
			} else {
				action += listener;
				Instance.playerDictionary.Add(name, action);
			}
		}

		public static void StartListening(EventName name, Action<float> listener) 
		{
			Action<float> action;
			if (Instance.intDictionary.TryGetValue(name, out action)) {
				action += listener;
				Instance.intDictionary[name] = action;
			} else {
				action += listener;
				Instance.intDictionary.Add(name, action);
			}
		}

		public static void StopListening(EventName name, Action<Player> listener) 
		{
			Action<Player> action;
			if (!Instance.playerDictionary.TryGetValue(name, out action)) {
				return;
			}

			action -= listener;
			Instance.playerDictionary[name] = action;
		}

		public static void StopListening(EventName name, Action listener) 
		{
			Action action;
			if (!Instance.voidDictionary.TryGetValue(name, out action)) return;
			
			action -= listener;
			Instance.voidDictionary[name] = action;
		}

		public static void StopListening(EventName name, Action<float> listener) 
		{
			Action<float> action;
			if (!Instance.intDictionary.TryGetValue(name, out action)) return;
			
			action -= listener;
			Instance.intDictionary[name] = action;
		}

		public static void TriggerEvent(EventName name, Player player) 
		{
			Action<Player> action;
			if (!Instance.playerDictionary.TryGetValue(name, out action)) return;

			if (action == null) return;

			action.Invoke(player);
		}

		public static void TriggerEvent(EventName name) 
		{
			Action action;
			if (!Instance.voidDictionary.TryGetValue(name, out action)) return;

			if (action == null) return;

			action.Invoke();
		}

		public static void TriggerEvent(EventName name, float number)
		{
			Action<float> action;
			if (!Instance.intDictionary.TryGetValue(name, out action)) return;

			if (action == null) return;

			action.Invoke(number);
		}
	}
}