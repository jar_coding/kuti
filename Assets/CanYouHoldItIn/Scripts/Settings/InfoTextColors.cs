﻿namespace CanYouHoldItIn.Scripts.Settings
{
	using UnityEngine;

	[CreateAssetMenu(menuName = "Settings/InfoTextColors")]
	public class InfoTextColors : ScriptableObject
	{
		public Color defaultColor;
		public Color startRoundColor;
		public Color countdownColor;
		public Color pressAnyKeyColor;
	}
}