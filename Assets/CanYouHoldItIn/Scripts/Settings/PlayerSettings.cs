﻿namespace CanYouHoldItIn.Scripts.Settings
{
	using UnityEngine;
	using Effects;

	[CreateAssetMenu(menuName = "Settings/PlayerSettings")]
	public class PlayerSettings : ScriptableObject
	{
		public float fartProbability = 0.9f;
		public float startScale = 1.4f;
		
		public ScaleEffect tickleScaling;
		
		[Header("Audio")]
		public AudioEffect laughtingAudio;
		public AudioEffect cryingAudio;

		[Header("Highlights")]
		public AnimationCurve highlightWinner;
		public float minimalSaturation = 0.6f;
	}
}