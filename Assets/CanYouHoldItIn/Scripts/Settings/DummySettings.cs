﻿namespace CanYouHoldItIn.Scripts.Settings
{
    using UnityEngine;

    [CreateAssetMenu(menuName = "Settings/DummySettings")]
    public class DummySettings : ScriptableObject
    {
        public Sprite[] dummies;
    }
}