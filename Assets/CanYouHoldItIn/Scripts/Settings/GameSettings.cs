﻿
namespace CanYouHoldItIn.Scripts.Settings
{
	using UnityEngine;
	
	[CreateAssetMenu(menuName = "Settings/Game")]
	public class GameSettings : ScriptableObject
	{
		public float timeToFillProgress = 0.5f;
		public float playTime = 10;
		public float delayBetweenRounds = 0.5f;
		public float delayAfterGameOver = 1f;
		[Range(1, 5)]
		public int numberOfPointsToWin;

		public AnimationCurve progress;
	}
}
