﻿namespace CanYouHoldItIn.Scripts
{
	using UnityEngine;

	public class AutoDestroy : MonoBehaviour
	{
		public float delay = 0.4f;

		private void Start()
		{
			Destroy(gameObject, delay);
		}
	}
}
